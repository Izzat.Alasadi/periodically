import static java.lang.Thread.sleep;

public class Periodically {


    Runnable r1=new Runnable() {
        @Override
        public void run() {
            System.out.println("Run() in r1 has been called ");
        }
    };

    public void periodically(int duration, Runnable f) throws InterruptedException{
        Thread t1=new Thread(f);
        System.out.println("Thread t1 has started");
        t1.start();

        System.out.println("Thread state: "+t1.getState());

        System.out.println("Thread t1 is going to sleep");
        sleep(duration);

        t1.stop();
        System.out.println("Thread t1 has been terminated ");
        System.out.println("Thread state: "+t1.getState());
    }
    public static void main(String[] args) throws InterruptedException {
        Periodically pr=new Periodically();
        while (true) {
            pr.periodically(5000, pr.r1);
        }
    }
}
